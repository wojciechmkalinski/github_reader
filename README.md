This project contain simple REST service which will return details of given Github repository. Details should
include:

        * full name of repository
        * description of repository
        * git clone url
        * number of stargazers
        * date of creation (ISO format)

Technologies used: Java 8 , Maven, Lombok, GJF, REST, Spring Boot


How to run application:

1. cd/to/project
2. If you are in location, which contains pom.xml, type 
    * mvn clean install 
to download any dependencies and clean is not a standard target goal and not executed automatically on every install.
3. After downloading all dependencies and running build successfully, write
    * mvn spring-boot:run
4. Go to your web browser, and go to address:
    * localhost:8080/repos/{name-of-repository-owner}/{name-of-repository}

It should return json with information about given repository.

If you want to run application tests, type:

    * mvn test

Notes:
    Project contains tests with an usage of Mockito.
    
You will find content of given project in branch_tech branch.
